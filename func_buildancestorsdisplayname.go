// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package convertfeed

import (
	"context"
	"regexp"

	"cloud.google.com/go/firestore"
	"gitlab.com/realtime-asset-monitor/utilities/glo"
)

// buildAncestorsDisplayName build a slice of Ancestor friendly name from a slice of ancestors
func buildAncestorsDisplayName(ctx context.Context,
	ancestors []string,
	collectionID string,
	firestoreClient *firestore.Client,
	cacheMaxAgeMinutes float64,
	ev glo.EntryValues) (ancestorsDisplayName []string, projectID string) {
	cnt := len(ancestors)
	ancestorsDisplayName = make([]string, len(ancestors))
	pattern, err := regexp.Compile("^organizations_.*|^folders_.*|projects_.*")
	if err != nil {
		glo.LogWarning(ev, "regexp.Compile", err.Error())
	} else {
		for idx := 0; idx < cnt; idx++ {
			displayName, p := getDisplayName(ctx,
				ancestors[idx],
				collectionID,
				firestoreClient,
				cacheMaxAgeMinutes,
				ev)

			if pattern.Match([]byte(displayName)) {
				glo.LogWarning(ev, "Ancestor friendly name not found", ancestors[idx])
			}
			ancestorsDisplayName[idx] = displayName
			if p != "" {
				projectID = p
			}
		}
	}
	return ancestorsDisplayName, projectID
}
