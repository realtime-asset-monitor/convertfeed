// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package convertfeed

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
	"testing"

	"cloud.google.com/go/firestore"
	"github.com/google/uuid"
	"gitlab.com/realtime-asset-monitor/utilities/cai"
	"gitlab.com/realtime-asset-monitor/utilities/gfs"
	"gitlab.com/realtime-asset-monitor/utilities/glo"
)

func TestIntegGetDisplayName(t *testing.T) {
	var testCases = []struct {
		name               string
		collectionID       string
		feedPath01         string
		feedPath02         string
		cacheMaxAgeMinutes float64
		ancestorName       string
		wantDisplayName    string
		wantProjectID      string
		wantMsgContains    string
		wantCacheLen       int
	}{
		{
			name:               "populate_1st_item_in_cache",
			collectionID:       "testcache01",
			feedPath01:         "testdata/cache_feeds_01.json",
			feedPath02:         "testdata/cache_feeds_02.json",
			cacheMaxAgeMinutes: 60,
			ancestorName:       "projects/123456789012",
			wantDisplayName:    "project01",
			wantProjectID:      "project01z",
			wantMsgContains:    "",
			wantCacheLen:       1,
		},
		{
			name:               "populate_2nd_item_in_cache",
			collectionID:       "testcache01",
			feedPath01:         "testdata/cache_feeds_01.json",
			feedPath02:         "testdata/cache_feeds_02.json",
			cacheMaxAgeMinutes: 60,
			ancestorName:       "projects/234567890121",
			wantDisplayName:    "project02",
			wantProjectID:      "project02z",
			wantMsgContains:    "",
			wantCacheLen:       2,
		},
		{
			name:               "project_outof_cache_and_outof_firestore",
			collectionID:       "testcache01",
			feedPath01:         "testdata/cache_feeds_01.json",
			feedPath02:         "testdata/cache_feeds_02.json",
			cacheMaxAgeMinutes: 60,
			ancestorName:       "projects/9999",
			wantDisplayName:    "projects_9999",
			wantProjectID:      "",
			wantMsgContains:    "not_found_in_firestore",
			wantCacheLen:       2,
		},
		{
			name:               "cache_too_old",
			collectionID:       "testcache01",
			feedPath01:         "testdata/cache_feeds_01.json",
			feedPath02:         "testdata/cache_feeds_02.json",
			cacheMaxAgeMinutes: 0,
			ancestorName:       "projects/123456789012",
			wantDisplayName:    "project01",
			wantProjectID:      "project01z",
			wantMsgContains:    "",
			wantCacheLen:       2,
		},
	}
	projectID := os.Getenv("CONVERTFEED_PROJECT_ID")
	if projectID == "" {
		t.Skip("Missing env var CONVERTFEED_PROJECT_ID")
	}

	firestoreClient, err := firestore.NewClient(ctx, projectID)
	if err != nil {
		log.Fatalf("firestore.NewClient %v", err)
	}

	var ev glo.EntryValues
	ev.CommonEntryValues.Environment = "test"
	ev.CommonEntryValues.InitID = fmt.Sprintf("%v", uuid.New())
	ev.CommonEntryValues.LogOnlySeveritylevels = "INFO WARNING NOTICE CRITICAL"
	ev.CommonEntryValues.MicroserviceName = "test"
	ev.Step.StepID = "0123456789"
	global.assetDNCache.Clear()

	for _, tc := range testCases {
		tc := tc // prevent G601
		t.Run(tc.name, func(t *testing.T) {
			p := filepath.Clean(tc.feedPath01)
			if !strings.HasPrefix(p, "testdata/") {
				panic(fmt.Errorf("Unsafe path %s", p)) // prevent G304
			}
			b, err := os.ReadFile(p)
			if err != nil {
				log.Fatalln(err)
			}
			var feedMessages []cai.FeedMessageFS
			err = json.Unmarshal(b, &feedMessages)
			if err != nil {
				log.Fatalln(err)
			}

			for _, feedMessage := range feedMessages {
				documentPath := tc.collectionID + "/" + strings.ReplaceAll(feedMessage.Asset.Name, "/", "\\")
				_, err = firestoreClient.Doc(documentPath).Set(ctx, feedMessage)
				if err != nil {
					log.Fatalf("firestore doc set %v", err)
				}
			}

			p = filepath.Clean(tc.feedPath02)
			if !strings.HasPrefix(p, "testdata/") {
				panic(fmt.Errorf("Unsafe path %s", p)) // prevent G304
			}
			b, err = os.ReadFile(p)
			if err != nil {
				log.Fatalln(err)
			}
			err = json.Unmarshal(b, &feedMessages)
			if err != nil {
				log.Fatalln(err)
			}

			for _, feedMessage := range feedMessages {
				documentPath := tc.collectionID + "/" + strings.ReplaceAll(feedMessage.Asset.Name, "/", "\\")
				_, err = firestoreClient.Doc(documentPath).Set(ctx, feedMessage)
				if err != nil {
					log.Fatalf("firestore doc set %v", err)
				}
			}

			var buffer bytes.Buffer
			log.SetOutput(&buffer)
			defer func() {
				log.SetOutput(os.Stderr)
			}()
			displayName, projectID := getDisplayName(ctx,
				tc.ancestorName,
				tc.collectionID,
				firestoreClient,
				tc.cacheMaxAgeMinutes,
				ev)
			msgString := buffer.String()
			// t.Logf("len %d %s", len(msgString), msgString)

			if tc.wantDisplayName != displayName {
				t.Errorf("want displayName %s got %s", tc.wantDisplayName, displayName)
			}
			if tc.wantProjectID != projectID {
				t.Errorf("want projectID %s got %s", tc.wantProjectID, projectID)
			}
			if !strings.Contains(msgString, tc.wantMsgContains) {
				t.Errorf("want msg to contains '%s' and got \n'%s'", tc.wantMsgContains, msgString)
			}
			if tc.wantCacheLen != global.assetDNCache.Count() {
				t.Errorf("want cache len '%d' and got '%d'", tc.wantCacheLen, global.assetDNCache.Count())
			}

			err = gfs.DeleteCollection(ctx,
				firestoreClient,
				firestoreClient.Collection(tc.collectionID),
				100)
			if err != nil {
				log.Fatalln(err)
			}
		})
	}
}
