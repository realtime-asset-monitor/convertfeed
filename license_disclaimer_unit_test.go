// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package convertfeed

import (
	"bytes"
	"os"
	"path/filepath"
	"testing"

	"gitlab.com/realtime-asset-monitor/utilities/str"
)

// TestUnitDisclaimer checks the source code starts with the license and disclaimer header
func TestUnitDisclaimer(t *testing.T) {
	err := filepath.Walk(".", func(path string, info os.FileInfo, err error) error {
		t.Run(filepath.Base(path), func(t *testing.T) {
			t.Parallel()
			if err != nil {
				t.Fatal(err)
			}
			if str.Find([]string{".go", ".yml", ".yaml"}, filepath.Ext(path)) {
				p := filepath.Clean(path) // prevent G304
				sourceCode, err := os.ReadFile(p)
				if err != nil {
					t.Fatal(err)
				}
				found := false
				switch filepath.Ext(path) {
				case ".go":
					found = bytes.HasPrefix(sourceCode, []byte(str.GODisclaimer))
				case ".yaml":
					found = bytes.HasPrefix(sourceCode, []byte(str.YAMLDisclaimer))
				case ".yml":
					found = bytes.HasPrefix(sourceCode, []byte(str.YAMLDisclaimer))
				}
				if !found {
					t.Errorf("%v: license and disclaimer header not found on first line", path)
				}
			}
		})
		return nil
	})
	if err != nil {
		t.Fatal(err)
	}
}
