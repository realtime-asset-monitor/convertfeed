// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//.

package convertfeed

import (
	"context"
	"encoding/json"
	"fmt"
	"regexp"
	"time"

	"cloud.google.com/go/pubsub"
	cloudevents "github.com/cloudevents/sdk-go/v2"
	"gitlab.com/realtime-asset-monitor/utilities/cai"
	"gitlab.com/realtime-asset-monitor/utilities/glo"
)

// EntryPoint function entry point.
func EntryPoint(ctxEvent context.Context, cloudEvent cloudevents.Event) error {
	var ev glo.EntryValues
	ev.CommonEntryValues = global.CommonEv
	ev.StepStack = make(glo.Steps, 0)
	ev.Step = glo.Step{
		StepID:        fmt.Sprintf("%s/%s", cloudEvent.Context.GetSource(), cloudEvent.Context.GetID()),
		StepTimestamp: cloudEvent.Context.GetTime(),
	}
	now := time.Now()
	d := now.Sub(ev.Step.StepTimestamp)

	b, _ := json.MarshalIndent(cloudEvent, "", "  ")
	glo.LogStartCloudEvent(ev, b, d.Seconds(), &now)

	pubsubData := &cai.WrappedPubSub{}
	if err := cloudEvent.DataAs(pubsubData); err != nil {
		glo.LogCriticalNoRetry(ev, fmt.Sprintf("cloudEvent.DataAs(pubsubData) %v", err), "", "")
		return nil
	}

	// end result must be a cai feed format
	var caiFeedMsg cai.FeedMessage
	var finishMsg string

	// fist case: if is already a cai feed format then enrich
	err := json.Unmarshal(pubsubData.Message.Data, &caiFeedMsg)
	if !(err != nil) {
		enrichCAIFeedMsg(ctxEvent,
			&caiFeedMsg,
			global.serviceEnv.AssetCollectionID,
			global.firestoreClient,
			global.serviceEnv.CacheMaxAgeMinutes,
			global.serviceEnv.OwnerLabelKeyName,
			global.serviceEnv.ViolationResolverLabelKeyName,
			ev)
		finishMsg = "enrichCAIFeedMsg"
	} else {
		glo.LogCriticalNoRetry(ev, fmt.Sprintf("unmanaged input message format %v", err), caiFeedMsg.Asset.AssetType, "")
		return nil
	}

	if caiFeedMsg.StepStack != nil {
		ev.StepStack = append(caiFeedMsg.StepStack, ev.Step)
	} else {
		var caiStep glo.Step
		caiStep.StepTimestamp = caiFeedMsg.Window.StartTime
		caiStep.StepID = fmt.Sprintf("%s/%s", caiFeedMsg.Asset.Name, caiStep.StepTimestamp.Format(time.RFC3339))
		ev.StepStack = append(ev.StepStack, caiStep)
		ev.StepStack = append(ev.StepStack, ev.Step)
	}
	caiFeedMsg.StepStack = ev.StepStack

	if len(string(caiFeedMsg.Asset.IamPolicy)) == 0 || string(caiFeedMsg.Asset.IamPolicy) == "null" {
		caiFeedMsg.ContentType = "RESOURCE"
	} else {
		caiFeedMsg.ContentType = "IAM_POLICY"
	}

	var pubSubMsg pubsub.Message
	pubSubMsg.Data, err = json.Marshal(&caiFeedMsg)
	if err != nil {
		glo.LogCriticalNoRetry(ev, fmt.Sprintf("json.Marshal(&caiFeedMsg) %v", err), "", "")
		return nil
	}

	pubSubMsg.Attributes = map[string]string{
		"assetType":        caiFeedMsg.Asset.AssetType,
		"contentType":      caiFeedMsg.ContentType,
		"messageType":      "asset_feed",
		"microserviceName": microserviceName,
		"origin":           caiFeedMsg.Origin,
	}

	cacheAge := now.Sub(global.attributesRepo.version)
	if global.serviceEnv.CacheMaxAgeMinutes == 0 || cacheAge.Minutes() > global.serviceEnv.CacheMaxAgeMinutes {
		global.attributesRepo, err = refreshCache(ctxEvent, global.attributesRepoBucket)
		if err != nil {
			glo.LogCriticalRetry(ev, fmt.Sprintf("refreshCache %v", err), "", "")
			return err
		}
	}
	assetTypeAndContent := fmt.Sprintf("%s%s", caiFeedMsg.Asset.AssetType, caiFeedMsg.ContentType)
	attributes, ok := global.attributesRepo.assetTypeAndContentAttributes[assetTypeAndContent]
	if ok {
		for pubSubAttributeName, pubSubAttributeValue := range attributes {
			pubSubMsg.Attributes[pubSubAttributeName] = pubSubAttributeValue
		}
	}
	pubSubMsg.Attributes["timestamp"] = now.Format(time.RFC3339)

	result := global.assetFeedTopic.Publish(ctxEvent, &pubSubMsg)
	//block until result is returned
	id, err := result.Get(ctxEvent)
	if err != nil {
		// https://cloud.google.com/pubsub/docs/reference/error-codes
		var re = regexp.MustCompile(`500|INTERNAL|502|BAD_GATEWAY|503|UNAVAILABLE|504|DEADLINE_EXCEEDED|`)
		if re.MatchString(err.Error()) {
			glo.LogCriticalRetry(ev, fmt.Sprintf("id %s %v", id, err), caiFeedMsg.Asset.AssetType, "")
			return err
		}
		glo.LogCriticalNoRetry(ev, fmt.Sprintf("id %s other errors %v", id, err), caiFeedMsg.Asset.AssetType, "")
		return nil
	}

	glo.LogFinish(ev, finishMsg, "", time.Now(), caiFeedMsg.Origin, caiFeedMsg.Asset.AssetType, caiFeedMsg.ContentType, "", 0)
	return nil
}
