// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package convertfeed

import (
	"context"
	"strings"
	"time"

	"cloud.google.com/go/firestore"
	"gitlab.com/realtime-asset-monitor/utilities/gfs"
	"gitlab.com/realtime-asset-monitor/utilities/glo"
	"gitlab.com/realtime-asset-monitor/utilities/str"
)

// getDisplayName retrieve the friendly name of an ancestor
func getDisplayName(ctx context.Context,
	name string,
	collectionID string,
	firestoreClient *firestore.Client,
	cacheMaxAgeMinutes float64,
	ev glo.EntryValues) (displayName string, projectID string) {
	displayName = strings.Replace(name, "/", "_", -1)
	projectID = ""
	ancestorType := strings.Split(name, "/")[0]
	knownAncestorTypes := []string{"organizations", "folders", "projects"}
	if !str.Find(knownAncestorTypes, ancestorType) {
		return displayName, projectID
	}
	fullName := "//cloudresourcemanager.googleapis.com/" + name
	var cacheFound, formatOk bool
	var displayNameID dnID
	var displayNameIDInterface interface{}
	displayNameIDInterface, cacheFound = global.assetDNCache.Get(fullName)
	if cacheFound {
		if displayNameID, formatOk = displayNameIDInterface.(dnID); formatOk {
			now := time.Now()
			cachedItemAge := now.Sub(displayNameID.TimeStamp)
			if cachedItemAge.Minutes() <= cacheMaxAgeMinutes {
				return displayNameID.DisplayName, displayNameID.ProjectID
			}
		}
	}
	// being here means not found or format_no_ok or found_too_old => need to request firestore

	var fsFound bool
	documentID := strings.ReplaceAll(fullName, "/", "\\")
	documentPath := collectionID + "/" + documentID
	documentSnap, getDocOk := gfs.GetDocWithRetry(ctx,
		firestoreClient,
		documentPath,
		global.serviceEnv.MaxRetry,
		false,
		ev)
	if getDocOk {
		assetMap := documentSnap.Data()
		var assetInterface interface{} = assetMap["asset"]
		if asset, ok := assetInterface.(map[string]interface{}); ok {
			var resourceInterface interface{} = asset["resource"]
			if resource, ok := resourceInterface.(map[string]interface{}); ok {
				var dataInterface interface{} = resource["data"]
				if data, ok := dataInterface.(map[string]interface{}); ok {
					switch ancestorType {
					case "organizations":
						var dNameInterface interface{} = data["displayName"]
						if dName, ok := dNameInterface.(string); ok {
							displayName = dName
							fsFound = true
						}
					case "folders":
						var dNameInterface interface{} = data["displayName"]
						if dName, ok := dNameInterface.(string); ok {
							displayName = dName
							fsFound = true
						}
					case "projects":
						var dNameInterface interface{} = data["name"]
						if dName, ok := dNameInterface.(string); ok {
							displayName = dName
							fsFound = true
						}
						var dProjectIDInterface interface{} = data["projectId"]
						if dProjectID, ok := dProjectIDInterface.(string); ok {
							projectID = dProjectID
						}
					}
				}
			}
		}
		if fsFound {
			var dnIDNew dnID
			dnIDNew.TimeStamp = time.Now()
			dnIDNew.DisplayName = displayName
			dnIDNew.ProjectID = projectID
			if cacheFound && formatOk {
				// means too old
				if displayName != displayNameID.DisplayName {
					// means too old and value has changed => update cache
					global.assetDNCache.Set(fullName, dnIDNew)
				}
			} else {
				global.assetDNCache.Set(fullName, dnIDNew)
			}
		} else {
			glo.LogInfo(ev, "DisplayName not found in Firestore document", documentPath)
		}
	}
	return displayName, projectID
}
