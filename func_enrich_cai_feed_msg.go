// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package convertfeed

import (
	"context"

	"cloud.google.com/go/firestore"
	"gitlab.com/realtime-asset-monitor/utilities/cai"
	"gitlab.com/realtime-asset-monitor/utilities/glo"
)

func enrichCAIFeedMsg(ctx context.Context,
	caiFeedMsg *cai.FeedMessage,
	collectionID string,
	firestoreClient *firestore.Client,
	cacheMaxAgeMinutes float64,
	ownerLabelKeyName string,
	violationResolverLabelKeyName string,
	ev glo.EntryValues) {

	if caiFeedMsg.Origin == "" {
		caiFeedMsg.Origin = "real-time"
	}
	caiFeedMsg.Asset.IamPolicyLegacy = caiFeedMsg.Asset.IamPolicy
	caiFeedMsg.Asset.AssetTypeLegacy = caiFeedMsg.Asset.AssetType

	caiFeedMsg.Asset.AncestryPath = buildAncestryPath(caiFeedMsg.Asset.Ancestors)
	caiFeedMsg.Asset.AncestryPathLegacy = caiFeedMsg.Asset.AncestryPath

	caiFeedMsg.Asset.AncestorsDisplayName, caiFeedMsg.Asset.ProjectID = buildAncestorsDisplayName(
		ctx,
		caiFeedMsg.Asset.Ancestors,
		collectionID,
		firestoreClient,
		cacheMaxAgeMinutes,
		ev)

	caiFeedMsg.Asset.AncestryPathDisplayName = buildAncestryPath(caiFeedMsg.Asset.AncestorsDisplayName)

	var owner, resolver string
	owner, err := getAssetLabelValue(ownerLabelKeyName, caiFeedMsg.Asset.Resource)
	if err != nil {
		glo.LogWarning(ev, "error getAssetLabelValue", err.Error())

	} else {
		resolver, _ = getAssetLabelValue(violationResolverLabelKeyName, caiFeedMsg.Asset.Resource)
	}
	caiFeedMsg.Asset.Owner = owner
	caiFeedMsg.Asset.ViolationResolver = resolver
}
