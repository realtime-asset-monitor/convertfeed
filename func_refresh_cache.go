// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package convertfeed

import (
	"context"
	"fmt"
	"io"
	"strings"
	"time"

	"cloud.google.com/go/storage"
	"google.golang.org/api/iterator"
	"gopkg.in/yaml.v2"
)

func refreshCache(ctx context.Context,
	attributesRepoBucket *storage.BucketHandle) (attributesRepository *attributesRepo, err error) {
	var repo attributesRepo
	repo.assetTypeAndContentAttributes = make(map[string]map[string]string)
	query := &storage.Query{Prefix: ""}
	it := attributesRepoBucket.Objects(ctx, query)
	for {
		attrs, err := it.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			return &repo, fmt.Errorf("it.Next() %v", err)
		}
		if strings.Contains(attrs.Name, ".yaml") {
			reader, err := attributesRepoBucket.Object(attrs.Name).NewReader(ctx)
			if err != nil {
				return &repo, fmt.Errorf("attributesRepoBucket.Object(attrs.Name).NewReader(ctx) %v", err)
			}
			defer reader.Close()
			b, err := io.ReadAll(reader)
			if err != nil {
				return &repo, fmt.Errorf("io.ReadAll(reader) %v", err)
			}
			var attributeSettings struct {
				PubSubAttributeName     string   `yaml:"pubSubAttributeName"`
				PubSubAttributeValue    string   `yaml:"pubSubAttributeValue"`
				AssetTypeAndContentList []string `yaml:"assetTypeAndContentList"`
			}
			err = yaml.Unmarshal(b, &attributeSettings)
			if err != nil {
				return &repo, fmt.Errorf("json.Unmarshal %v", err)
			}
			// fmt.Printf("PubSubAttributeName %s PubSubAttributeValue %s\n", attributeSettings.PubSubAttributeName, attributeSettings.PubSubAttributeValue)
			for _, assetTypeAndContent := range attributeSettings.AssetTypeAndContentList {
				// When no content type default to RESOURCE
				if !strings.Contains(assetTypeAndContent, "IAM_POLICY") {
					if !strings.Contains(assetTypeAndContent, "RESOURCE") {
						assetTypeAndContent = fmt.Sprintf("%sRESOURCE", assetTypeAndContent)
					}
				}
				// fmt.Println(assetTypeAndContent)
				attributes := make(map[string]string)
				if repo.assetTypeAndContentAttributes[assetTypeAndContent] != nil {
					attributes = repo.assetTypeAndContentAttributes[assetTypeAndContent]
				} else {
					repo.assetTypeAndContentAttributes[assetTypeAndContent] = make(map[string]string)
				}
				attributes[attributeSettings.PubSubAttributeName] = attributeSettings.PubSubAttributeValue
				repo.assetTypeAndContentAttributes[assetTypeAndContent] = attributes
			}
		}
	}
	repo.version = time.Now()
	return &repo, nil
}
