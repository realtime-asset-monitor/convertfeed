# convertfeed

![pipeline status](https://gitlab.com/realtime-asset-monitor/convertfeed/badges/main/pipeline.svg) ![coverage](https://gitlab.com/realtime-asset-monitor/convertfeed/badges/main/coverage.svg?min_good=80)  
Convert a message to an asset feed format expected by RAM

## Prerequsites

Manual task:

- From Firestore console, select `native mode` and the multi-region location of your choice (e.g. eur3)

For running integration test in the DEV environment:

- `roles/datastore.user` instead of `roles/datastore.viewer`
