// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package convertfeed

import (
	"time"

	"cloud.google.com/go/firestore"
	"cloud.google.com/go/pubsub"
	"cloud.google.com/go/storage"
	cmap "github.com/orcaman/concurrent-map"
	"gitlab.com/realtime-asset-monitor/utilities/glo"
)

const microserviceName = "convertfeed"

// ServiceEnv list environment variables prefixed with the name of the microservice
type ServiceEnv struct {
	AssetCollectionID             string  `envconfig:"asset_collection_id" default:"assets"`
	AssetFeedTopicID              string  `envconfig:"asset_feed_topic_id" default:"assetFeed"`
	AttributesRepoBucketName      string  `envconfig:"attributes_repo_bucket_name"`
	CacheMaxAgeMinutes            float64 `envconfig:"cache_max_age_minutes" default:"60"`
	Environment                   string  `envconfig:"environment" default:"dev"`
	LogOnlySeveritylevels         string  `envconfig:"log_only_severity_levels" default:"WARNING NOTICE CRITICAL"`
	MaxRetry                      int     `envconfig:"max_retry" default:"10"`
	OwnerLabelKeyName             string  `envconfig:"owner_label_Key_name" default:"owner"`
	ProjectID                     string  `envconfig:"project_id" required:"true"`
	StartProfiler                 bool    `envconfig:"start_profiler" default:"false"`
	ViolationResolverLabelKeyName string  `envconfig:"violation_resolver_label_key_name" default:"resolver"`
}

// Env list environment variables
type Env struct {
	KConfiguration string `envconfig:"k_configuration"`
	KRevision      string `envconfig:"k_revision"`
	KService       string `envconfig:"k_service"`
}

type dnID struct {
	DisplayName string `json:"displayName"`
	ProjectID   string `json:"projectID"`
	TimeStamp   time.Time
}

// Global structure for global variables to optimize the performances in serverless mode
type Global struct {
	assetDNCache         cmap.ConcurrentMap
	assetFeedTopic       pubsub.Topic
	attributesRepo       *attributesRepo
	attributesRepoBucket *storage.BucketHandle
	CommonEv             glo.CommonEntryValues
	env                  *Env
	firestoreClient      *firestore.Client
	serviceEnv           *ServiceEnv
}

type attributesRepo struct {
	assetTypeAndContentAttributes map[string]map[string]string
	version                       time.Time
}
