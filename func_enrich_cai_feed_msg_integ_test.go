// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package convertfeed

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"reflect"
	"strings"
	"testing"

	"cloud.google.com/go/firestore"
	"github.com/google/uuid"
	"gitlab.com/realtime-asset-monitor/utilities/cai"
	"gitlab.com/realtime-asset-monitor/utilities/gfs"
	"gitlab.com/realtime-asset-monitor/utilities/glo"
)

func TestIntegEnrichCAIFeedMsg(t *testing.T) {
	var testCases = []struct {
		name                        string
		feedPath                    string
		collectionID                string
		feedsPath                   string
		cacheMaxAgeMinutes          float64
		wantAncestryPath            string
		wantAncestorsDisplayName    []string
		wantProjectID               string
		wantAncestryPathDisplayName string
		wantOwner                   string
		wantResolver                string
	}{
		{
			name:               "bq_dataset_real_time",
			feedPath:           "testdata/cai_feed_01.json",
			collectionID:       "testcache01",
			feedsPath:          "testdata/cache_feeds_01.json",
			cacheMaxAgeMinutes: 60,
			wantAncestryPath:   "organization/123456789012/folder/123456789012/project/123456789012",
			wantAncestorsDisplayName: []string{
				"project01", "foldera", "ramtests.test"},
			wantProjectID:               "project01z",
			wantAncestryPathDisplayName: "ramtests.test/foldera/project01",
			wantOwner:                   "justine",
			wantResolver:                "patrick",
		},
		{
			name:               "iam_pol_project",
			feedPath:           "testdata/cai_feed_02.json",
			collectionID:       "testcache01",
			feedsPath:          "testdata/cache_feeds_01.json",
			cacheMaxAgeMinutes: 60,
		},
	}
	projectID := os.Getenv("CONVERTFEED_PROJECT_ID")
	if projectID == "" {
		t.Skip("Missing env var CONVERTFEED_PROJECT_ID")
	}

	firestoreClient, err := firestore.NewClient(ctx, projectID)
	if err != nil {
		log.Fatalf("firestore.NewClient %v", err)
	}

	var ev glo.EntryValues
	ev.CommonEntryValues.Environment = "test"
	ev.CommonEntryValues.InitID = fmt.Sprintf("%v", uuid.New())
	ev.CommonEntryValues.LogOnlySeveritylevels = "INFO WARNING NOTICE CRITICAL"
	ev.CommonEntryValues.MicroserviceName = "test"
	ev.Step.StepID = "0123456789"

	for _, tc := range testCases {
		tc := tc // prevent G601
		t.Run(tc.name, func(t *testing.T) {
			p := filepath.Clean(tc.feedPath)
			if !strings.HasPrefix(p, "testdata/") {
				panic(fmt.Errorf("Unsafe path %s", p)) // prevent G304
			}
			b, err := os.ReadFile(p)
			if err != nil {
				log.Fatalln(err)
			}
			var caiFeedMsg cai.FeedMessage
			err = json.Unmarshal(b, &caiFeedMsg)
			if err != nil {
				log.Fatalln(err)
			}
			p = filepath.Clean(tc.feedsPath)
			if !strings.HasPrefix(p, "testdata/") {
				panic(fmt.Errorf("Unsafe path %s", p)) // prevent G304
			}
			b, err = os.ReadFile(p)
			if err != nil {
				log.Fatalln(err)
			}
			var feedMessages []cai.FeedMessageFS
			err = json.Unmarshal(b, &feedMessages)
			if err != nil {
				log.Fatalln(err)
			}

			for _, feedMessage := range feedMessages {
				documentPath := tc.collectionID + "/" + strings.ReplaceAll(feedMessage.Asset.Name, "/", "\\")
				_, err = firestoreClient.Doc(documentPath).Set(ctx, feedMessage)
				if err != nil {
					log.Fatalf("firestore doc set %v", err)
				}
			}

			enrichCAIFeedMsg(ctx,
				&caiFeedMsg,
				tc.collectionID,
				firestoreClient,
				tc.cacheMaxAgeMinutes,
				"owner",
				"resolver",
				ev)

			if caiFeedMsg.Origin == "" {
				t.Errorf("Origin should not be empty")
			}
			if tc.wantAncestryPath != "" && caiFeedMsg.Asset.AncestryPath != tc.wantAncestryPath {
				t.Errorf("Want ancestry path %s got %s", tc.wantAncestryPath, caiFeedMsg.Asset.AncestryPath)
			}
			if len(tc.wantAncestorsDisplayName) > 0 && !reflect.DeepEqual(tc.wantAncestorsDisplayName, caiFeedMsg.Asset.AncestorsDisplayName) {
				t.Errorf("want ancestorDisplayName %v got %v", tc.wantAncestorsDisplayName, caiFeedMsg.Asset.AncestorsDisplayName)
			}
			if tc.wantProjectID != caiFeedMsg.Asset.ProjectID {
				t.Errorf("want projectID %s got %s", tc.wantProjectID, caiFeedMsg.Asset.ProjectID)
			}
			if tc.wantAncestryPathDisplayName != "" && tc.wantAncestryPathDisplayName != caiFeedMsg.Asset.AncestryPathDisplayName {
				t.Errorf("want ancestryPathDisplayName %s got %v", tc.wantAncestryPathDisplayName, caiFeedMsg.Asset.AncestryPathDisplayName)
			}
			if tc.wantOwner != caiFeedMsg.Asset.Owner {
				t.Errorf("want owner %s got %v", tc.wantOwner, caiFeedMsg.Asset.Owner)
			}
			if tc.wantResolver != caiFeedMsg.Asset.ViolationResolver {
				t.Errorf("want resolver %s got %v", tc.wantResolver, caiFeedMsg.Asset.ViolationResolver)
			}
			if caiFeedMsg.Asset.AncestryPathLegacy != caiFeedMsg.Asset.AncestryPath {
				t.Errorf("AncestryPathLegacy is NOT equal to AncestryPath")
			}
			if caiFeedMsg.Asset.AssetTypeLegacy != caiFeedMsg.Asset.AssetType {
				t.Errorf("AssetTypeLegacy is NOT equal to AssetType")
			}
			if len(string(caiFeedMsg.Asset.IamPolicy)) > 0 && len(string(caiFeedMsg.Asset.IamPolicyLegacy)) == 0 {
				t.Errorf("Asset.IamPolicyLegacy should be populated as Asset.IamPolicy is not null")
			}

			err = gfs.DeleteCollection(ctx,
				firestoreClient,
				firestoreClient.Collection(tc.collectionID),
				100)
			if err != nil {
				log.Fatalln(err)
			}

		})
	}

}
