// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package convertfeed

import (
	"bytes"
	"log"
	"os"
	"strings"
	"testing"
)

func TestIntegInitialize(t *testing.T) {
	projectID := os.Getenv("CONVERTFEED_PROJECT_ID")
	if projectID == "" {
		t.Skip("Missing env var CONVERTFEED_PROJECT_ID")
	}
	t.Run("initialize", func(t *testing.T) {
		var buffer bytes.Buffer
		log.SetOutput(&buffer)
		defer func() {
			log.SetOutput(os.Stderr)
		}()
		initialize()
		msgString := buffer.String()
		wantMsgContains := "\"severity\":\"INFO\",\"message\":\"init done\",\"init_id\":"
		if !strings.Contains(msgString, wantMsgContains) {
			t.Errorf("want msg to contains '%s' and got \n'%s'", wantMsgContains, msgString)
		}

	})

}
