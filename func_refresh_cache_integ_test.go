// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//.

package convertfeed

import (
	"bytes"
	"context"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
	"testing"

	"cloud.google.com/go/storage"
	"google.golang.org/api/iterator"
)

func TestIntegRefreshCache(t *testing.T) {
	var testCases = []struct {
		name                                string
		folderPath                          string
		wantErrorMsgContains                string
		wantLogMsgContains                  string
		wantAssetAndContentTypeCount        int
		wantAssetAndContentTypeActionCounts map[string]int
	}{
		{
			name:                         "emptybucket",
			folderPath:                   "testdata/attributes00",
			wantAssetAndContentTypeCount: 0,
		},
		{
			name:                         "publish2fs",
			folderPath:                   "testdata/attributes01",
			wantAssetAndContentTypeCount: 12,
			wantAssetAndContentTypeActionCounts: map[string]int{
				"cloudresourcemanager.googleapis.com/FolderRESOURCE":       1,
				"cloudresourcemanager.googleapis.com/OrganizationRESOURCE": 1,
				"cloudresourcemanager.googleapis.com/ProjectRESOURCE":      1,
				"run.googleapis.com/ServiceRESOURCE":                       1,
				"run.googleapis.com/ServiceIAM_POLICY":                     1,
				"compute.googleapis.com/GlobalForwardingRuleRESOURCE":      1,
				"compute.googleapis.com/GlobalAddressRESOURCE":             1,
				"compute.googleapis.com/TargetHttpsProxyRESOURCE":          1,
				"compute.googleapis.com/SslCertificateRESOURCE":            1,
				"compute.googleapis.com/UrlMapRESOURCE":                    1,
				"compute.googleapis.com/BackendServiceRESOURCE":            1,
				"compute.googleapis.com/NetworkEndpointGroupRESOURCE":      1,
			},
		},
		{
			name:                         "publish2fs_and_upload2gcs",
			folderPath:                   "testdata/attributes02",
			wantAssetAndContentTypeCount: 12,
			wantAssetAndContentTypeActionCounts: map[string]int{
				"cloudresourcemanager.googleapis.com/FolderRESOURCE":       1,
				"cloudresourcemanager.googleapis.com/OrganizationRESOURCE": 1,
				"cloudresourcemanager.googleapis.com/ProjectRESOURCE":      2,
				"run.googleapis.com/ServiceRESOURCE":                       2,
				"run.googleapis.com/ServiceIAM_POLICY":                     2,
				"compute.googleapis.com/GlobalForwardingRuleRESOURCE":      1,
				"compute.googleapis.com/GlobalAddressRESOURCE":             1,
				"compute.googleapis.com/TargetHttpsProxyRESOURCE":          1,
				"compute.googleapis.com/SslCertificateRESOURCE":            1,
				"compute.googleapis.com/UrlMapRESOURCE":                    1,
				"compute.googleapis.com/BackendServiceRESOURCE":            1,
				"compute.googleapis.com/NetworkEndpointGroupRESOURCE":      1,
			},
		},
	}
	projectID := os.Getenv("CONVERTFEED_PROJECT_ID")
	if projectID == "" {
		t.Skip("Missing env var CONVERTFEED_PROJECT_ID")
	}
	ctx := context.Background()
	storageClient, err := storage.NewClient(ctx)
	if err != nil {
		log.Fatalf("storage.NewClient %v", err)
	}
	attributesRepoBucketName := projectID + "-attributesrepo"
	attributesRepoBucket := storageClient.Bucket(attributesRepoBucketName)

	for _, tc := range testCases {
		tc := tc // https://github.com/golang/go/wiki/CommonMistakes#using-goroutines-on-loop-iterator-variables
		t.Run(tc.name, func(t *testing.T) {
			query := &storage.Query{Prefix: ""}
			it := attributesRepoBucket.Objects(ctx, query)
			for {
				attrs, err := it.Next()
				if err == iterator.Done {
					break
				}
				if err != nil {
					log.Fatalf("it.Next() %s", err)
				}
				err = attributesRepoBucket.Object(attrs.Name).Delete(ctx)
				if err != nil {
					log.Fatalf("attributesRepoBucket.Object(attrs.Name).Delete(ctx) %s", err)
				}
				t.Logf("deleted test data %s %s", attributesRepoBucketName, attrs.Name)
			}
			err := filepath.Walk(tc.folderPath, func(path string, info os.FileInfo, err error) error {
				if filepath.Ext(path) == ".yaml" {
					p := filepath.Clean(path)
					if !strings.HasPrefix(p, "testdata/") {
						panic(fmt.Errorf("Unsafe path %s", p)) // prevent G304
					}
					b, err := os.ReadFile(p)
					if err != nil {
						return err
					}
					storageObject := attributesRepoBucket.Object(filepath.Base(path))
					storageObjectWriter := storageObject.NewWriter(context.Background())
					_, err = fmt.Fprint(storageObjectWriter, string(b))
					if err != nil {
						return err
					}
					err = storageObjectWriter.Close()
					if err != nil {
						return err
					}
					t.Logf("find %s, added %s to %s", path, storageObject.ObjectName(), storageObject.BucketName())
				}
				return nil
			})
			if err != nil {
				log.Fatal(err)
			}

			var buffer bytes.Buffer
			log.SetOutput(&buffer)
			defer func() {
				log.SetOutput(os.Stderr)
			}()
			repo, err := refreshCache(ctx,
				attributesRepoBucket)
			msgString := buffer.String()
			// t.Logf("%v", msgString)

			if tc.wantErrorMsgContains == "" {
				if err != nil {
					t.Errorf("want no errors and got %v", err)
				}
				if !strings.Contains(msgString, tc.wantLogMsgContains) {
					t.Errorf("want log msg contains %s and did not get it", tc.wantLogMsgContains)
				}
				if len(repo.assetTypeAndContentAttributes) != tc.wantAssetAndContentTypeCount {
					t.Errorf("want %d AssetAndContentTypes and got %d", tc.wantAssetAndContentTypeCount, len(repo.assetTypeAndContentAttributes))
				}
				for AssetAndContentTypeName, actionCount := range tc.wantAssetAndContentTypeActionCounts {
					if len(repo.assetTypeAndContentAttributes[AssetAndContentTypeName]) != actionCount {
						t.Errorf("AssetAndContentType %s want %d attributes and got %d", AssetAndContentTypeName, actionCount, len(repo.assetTypeAndContentAttributes[AssetAndContentTypeName]))
					}
				}
			} else {
				if !strings.Contains(err.Error(), tc.wantErrorMsgContains) {
					t.Errorf("want error msg %s and got %s", tc.wantErrorMsgContains, err.Error())
				}
			}
			it = attributesRepoBucket.Objects(ctx, query)
			for {
				attrs, err := it.Next()
				if err == iterator.Done {
					break
				}
				if err != nil {
					log.Fatalf("it.Next() %s", err)
				}
				err = attributesRepoBucket.Object(attrs.Name).Delete(ctx)
				if err != nil {
					log.Fatalf("attributesRepoBucket.Object(attrs.Name).Delete(ctx) %s", err)
				}
				t.Logf("deleted test data %s %s", attributesRepoBucketName, attrs.Name)
			}

		})
	}
}
