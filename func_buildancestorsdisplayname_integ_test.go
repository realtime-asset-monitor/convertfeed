// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package convertfeed

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"reflect"
	"strings"
	"testing"

	"cloud.google.com/go/firestore"
	"github.com/google/uuid"
	"gitlab.com/realtime-asset-monitor/utilities/cai"
	"gitlab.com/realtime-asset-monitor/utilities/gfs"
	"gitlab.com/realtime-asset-monitor/utilities/glo"
)

func TestIntegBuildAncestorsDisplayName(t *testing.T) {
	var testCases = []struct {
		name                     string
		collectionID             string
		feedPath01               string
		feedPath02               string
		cacheMaxAgeMinutes       float64
		ancestors                []string
		wantAncestorsDisplayName []string
		wantProjectID            string
		wantMsgContains          string
	}{
		{
			name:               "ancestors_dn_known",
			collectionID:       "testcache01",
			feedPath01:         "testdata/cache_feeds_01.json",
			feedPath02:         "testdata/cache_feeds_02.json",
			cacheMaxAgeMinutes: 60,
			ancestors: []string{
				"projects/123456789012",
				"folders/123456789012",
				"organizations/123456789012"},
			wantProjectID: "project01z",
			wantAncestorsDisplayName: []string{
				"project01", "foldera", "ramtests.test"},
		},
		{
			name:               "ancestors_dn_partially_known",
			collectionID:       "testcache01",
			feedPath01:         "testdata/cache_feeds_01.json",
			feedPath02:         "testdata/cache_feeds_02.json",
			cacheMaxAgeMinutes: 60,
			ancestors: []string{
				"projects/9999",
				"folders/123456789012",
				"organizations/123456789012"},
			wantProjectID: "",
			wantAncestorsDisplayName: []string{
				"projects_9999", "foldera", "ramtests.test"},
			wantMsgContains: "Ancestor friendly name not found",
		},
	}
	projectID := os.Getenv("CONVERTFEED_PROJECT_ID")
	if projectID == "" {
		t.Skip("Missing env var CONVERTFEED_PROJECT_ID")
	}

	firestoreClient, err := firestore.NewClient(ctx, projectID)
	if err != nil {
		log.Fatalf("firestore.NewClient %v", err)
	}

	var ev glo.EntryValues
	ev.CommonEntryValues.Environment = "test"
	ev.CommonEntryValues.InitID = fmt.Sprintf("%v", uuid.New())
	ev.CommonEntryValues.LogOnlySeveritylevels = "INFO WARNING NOTICE CRITICAL"
	ev.CommonEntryValues.MicroserviceName = "test"
	ev.Step.StepID = "0123456789"

	for _, tc := range testCases {
		tc := tc // prevent G601
		t.Run(tc.name, func(t *testing.T) {
			p := filepath.Clean(tc.feedPath01)
			if !strings.HasPrefix(p, "testdata/") {
				panic(fmt.Errorf("Unsafe path %s", p)) // prevent G304
			}
			b, err := os.ReadFile(p)
			if err != nil {
				log.Fatalln(err)
			}
			var feedMessages []cai.FeedMessageFS
			err = json.Unmarshal(b, &feedMessages)
			if err != nil {
				log.Fatalln(err)
			}

			for _, feedMessage := range feedMessages {
				documentPath := tc.collectionID + "/" + strings.ReplaceAll(feedMessage.Asset.Name, "/", "\\")
				_, err = firestoreClient.Doc(documentPath).Set(ctx, feedMessage)
				if err != nil {
					log.Fatalf("firestore doc set %v", err)
				}
			}

			p = filepath.Clean(tc.feedPath02)
			if !strings.HasPrefix(p, "testdata/") {
				panic(fmt.Errorf("Unsafe path %s", p)) // prevent G304
			}
			b, err = os.ReadFile(p)
			if err != nil {
				log.Fatalln(err)
			}

			err = json.Unmarshal(b, &feedMessages)
			if err != nil {
				log.Fatalln(err)
			}

			for _, feedMessage := range feedMessages {
				documentPath := tc.collectionID + "/" + strings.ReplaceAll(feedMessage.Asset.Name, "/", "\\")
				_, err = firestoreClient.Doc(documentPath).Set(ctx, feedMessage)
				if err != nil {
					log.Fatalf("firestore doc set %v", err)
				}
			}

			var buffer bytes.Buffer
			log.SetOutput(&buffer)
			defer func() {
				log.SetOutput(os.Stderr)
			}()

			ancestorsDisplayName, projectID := buildAncestorsDisplayName(ctx,
				tc.ancestors,
				tc.collectionID,
				firestoreClient,
				tc.cacheMaxAgeMinutes,
				ev)

			msgString := buffer.String()
			// t.Logf("len %d %s", len(msgString), msgString)

			if !strings.Contains(msgString, tc.wantMsgContains) {
				t.Errorf("want msg to contains '%s' and got \n'%s'", tc.wantMsgContains, msgString)
			}

			if !reflect.DeepEqual(tc.wantAncestorsDisplayName, ancestorsDisplayName) {
				t.Errorf("want ancestorDisplayName %v got %v", tc.wantAncestorsDisplayName, ancestorsDisplayName)
			}

			if tc.wantProjectID != projectID {
				t.Errorf("want projectID %s got %s", tc.wantProjectID, projectID)
			}

			err = gfs.DeleteCollection(ctx,
				firestoreClient,
				firestoreClient.Collection(tc.collectionID),
				100)
			if err != nil {
				log.Fatalln(err)
			}
		})
	}
}
