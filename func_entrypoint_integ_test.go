// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package convertfeed

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
	"testing"
	"time"

	"cloud.google.com/go/storage"
	cloudevents "github.com/cloudevents/sdk-go/v2"
	"gitlab.com/realtime-asset-monitor/utilities/cai"
	"gitlab.com/realtime-asset-monitor/utilities/glo"
	"google.golang.org/api/iterator"
)

func TestIntegEntryPoint(t *testing.T) {
	testCases := []struct {
		name            string
		feedPath        string
		folderPath      string
		wantMsgContains string
	}{
		{
			name:            "feed_bq_dataset_resource",
			feedPath:        "testdata/cai_feed_01.json",
			folderPath:      "testdata/attributes01",
			wantMsgContains: "finish",
		},
		{
			name:            "feed_rcemgr_project_iam",
			feedPath:        "testdata/cai_feed_02.json",
			folderPath:      "testdata/attributes01",
			wantMsgContains: "finish",
		},
		{
			name:            "feed_rcemgr_project_rce_metadata",
			feedPath:        "testdata/cai_feed_03.json",
			folderPath:      "testdata/attributes02",
			wantMsgContains: "finish",
		},
	}
	projectID := os.Getenv("CONVERTFEED_PROJECT_ID")
	if projectID == "" {
		t.Skip("Missing env var CONVERTFEED_PROJECT_ID")
	}
	storageClient, err := storage.NewClient(ctx)
	if err != nil {
		log.Fatalf("storage.NewClient %v", err)
	}
	attributesRepoBucketName := projectID + "-attributesrepo"
	attributesRepoBucket := storageClient.Bucket(attributesRepoBucketName)

	now := time.Now()
	var cloudEvent cloudevents.Event
	cloudEvent.SetSpecVersion("1.0")
	cloudEvent.SetID("3558561719421540")
	cloudEvent.SetSource("//pubsub.googleapis.com/projects/qwerty-ram-dev-100/topics/caiFeed")
	cloudEvent.SetType("google.cloud.pubsub.topic.v1.messagePublished")
	cloudEvent.SetDataContentType("application/json")
	cloudEvent.SetTime(now)

	global.serviceEnv.CacheMaxAgeMinutes = 0

	for _, tc := range testCases {
		tc := tc // prevent G601
		t.Run(tc.name, func(t *testing.T) {
			query := &storage.Query{Prefix: ""}
			it := attributesRepoBucket.Objects(ctx, query)
			for {
				attrs, err := it.Next()
				if err == iterator.Done {
					break
				}
				if err != nil {
					log.Fatalf("it.Next() %s", err)
				}
				err = attributesRepoBucket.Object(attrs.Name).Delete(ctx)
				if err != nil {
					log.Fatalf("attributesRepoBucket.Object(attrs.Name).Delete(ctx) %s", err)
				}
				t.Logf("deleted test data %s %s", attributesRepoBucketName, attrs.Name)
			}
			err := filepath.Walk(tc.folderPath, func(path string, info os.FileInfo, err error) error {
				if filepath.Ext(path) == ".yaml" {
					p := filepath.Clean(path)
					if !strings.HasPrefix(p, "testdata/") {
						panic(fmt.Errorf("Unsafe path %s", p)) // prevent G304
					}
					b, err := os.ReadFile(p)
					if err != nil {
						return err
					}
					storageObject := attributesRepoBucket.Object(filepath.Base(path))
					storageObjectWriter := storageObject.NewWriter(context.Background())
					_, err = fmt.Fprint(storageObjectWriter, string(b))
					if err != nil {
						return err
					}
					err = storageObjectWriter.Close()
					if err != nil {
						return err
					}
					t.Logf("find %s, added %s to %s", path, storageObject.ObjectName(), storageObject.BucketName())
				}
				return nil
			})
			if err != nil {
				log.Fatal(err)
			}

			p := filepath.Clean(tc.feedPath)
			if !strings.HasPrefix(p, "testdata/") {
				panic(fmt.Errorf("Unsafe path %s", p)) // prevent G304
			}
			b, err := os.ReadFile(p)
			if err != nil {
				log.Fatalln(err)
			}
			var caiFeedMsg cai.FeedMessage
			err = json.Unmarshal(b, &caiFeedMsg)
			if err != nil {
				log.Fatalln(err)
			}

			pubsubData := &cai.WrappedPubSub{}
			pubsubData.Message.ID = cloudEvent.ID()
			pubsubData.Subscription = "projects/qwerty-ram-dev-100/subscriptions/eventarc-europe-west1-convertfeed-sub-999"
			pubsubData.Message.Data = b

			cloudEvent.SetData("application/json", pubsubData)

			var buffer bytes.Buffer
			log.SetOutput(&buffer)
			defer func() {
				log.SetOutput(os.Stderr)
			}()
			err = EntryPoint(ctx, cloudEvent)
			if err != nil {
				t.Errorf("want no error and got %v", err)
			}
			msgString := buffer.String()
			if !strings.Contains(msgString, tc.wantMsgContains) {
				t.Errorf("want msg to contains '%s' and got \n'%s'", tc.wantMsgContains, msgString)
			}
			logEntriesString := strings.Split(msgString, "\n")
			for _, logEntryString := range logEntriesString {
				var logEntry glo.Entry
				err = json.Unmarshal([]byte(logEntryString), &logEntry)
				if !(err != nil) {
					if logEntry.MicroserviceName == "" {
						t.Errorf("logEntry.MicroserviceName is null should not %s", logEntry.Message)
					}
					if logEntry.Environment == "" {
						t.Errorf("logEntry.Environment is null should not %s", logEntry.Message)
					}
					if strings.HasPrefix(strings.ToLower(logEntry.Message), "finish") {
						if len(logEntry.StepStack) == 0 {
							t.Errorf("logEntry.StepStack len is zero and should not %s", logEntry.Message)
						}
						if logEntry.LatencyE2ESeconds == 0 {
							t.Errorf("logEntry.LatencyE2ESeconds is zero and should not %s", logEntry.Message)
						}
						if logEntry.LatencySeconds == 0 {
							t.Errorf("logEntry.LatencySeconds is zero and should not %s", logEntry.Message)
						}
					}
				}
			}
		})
	}
}
